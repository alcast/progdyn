# ProgDyn
Auteur : Albane Castillon

## But du Projet 

- Exercice 1 : Divide and conquer  
Implémentation de la fonction par dichotomie.    
La fonction aura un tableau random et trié avec le tri heapsort.

- Exercice 2 : Algorithme glouton  
Implementation de la fonction Knapsack (problème de sac à dos).  
La fonction sera trié avec le tri de selection.

## Création de documentation
Le code est documenté et annoté. Pour générer la documentation, la commande sera écrite dans le dossier makefile.
La commande : ```make documentation```
