/**
 * file : utils.c
 * author : albane castillon
 * date : 20/10/2021
 * version : 1
*/

#include <stdio.h>
#include <stdlib.h>
#include "utils.h"

/**
 * @brief créer le tableau et tri les tri par ordre croissant
 *
 * @param tab tableau de valeur int
 * @param size int longueur du tableau
*/
void fillandsorttab(int tab[], int size)
{
    filltab(tab, size);
    heapsort(tab, size);
}

/**
 * @brief créé un tableau avec des valeurs random
 * 
 * @param tab tableau de valeur int
 * @param taille taille du tableau 
 */
void filltab(int *tab, int taille)
{
    srand(time(NULL)); // création d'une valeure random
    for (int i = 0; i < taille - 1; i++)
    {
        tab[i] = (int)(rand() % 100);
    }
}

void swap(int *a, int *b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

/**
 * @brief tri les valeurs par avec le tri par tas
 * 
 * @param tab tableau int a trier
 * @param size taille du tableau
 * @param i 
 */
void heapify(int *tab, int size, int i)
{
    int largest = i;
    int left = 2 * i + 1;
    int right = 2 * i + 2;

    if (left < size && (tab[left] > tab[largest]))
        largest = left;

    if (right < size && (tab[right] > tab[largest]))
        largest = right;

    if (largest != i)
    {
        swap(&tab[i], &tab[largest]);
        heapify(tab, size, largest);
    }
}

/**
 * @brief tri les valeurs par avec le tri par tas
 * 
 * @param tab tableau int a trier
 * @param size taille du tableau
 */
void heapsort(int *tab, int size)
{
    for (int i = size / 2 - 1; i >= 0; i--)
    {
        heapify(tab, size, i);
    }
    for (int i = size - 1; i >= 0; i--)
    {
        swap(&tab[0], &tab[i]);
        heapify(tab, i, 0);
    }
}
