/**
 * file : dichotomie.c
 * author : albane castillon
 * date : 20/10/2021
 * version : 1
*/

#include "dichotomie.h"

/**
* Recherche par dichotomie dans un tableau d'entiers
* @param array The array of values
* @param size_t The size of the array
* @param value The value to find
* @return The position of the value found or -1
*/
int find_by_dichotomy(int array[], int size_t, int value)
{
    int maxi = size_t - 1;
    int mini = 0;
    int m;

    while (mini <= maxi)
    {
        m = (maxi + mini) / 2;
        if (value == array[m])
        {
            return m;
        }
        else
        {
            if (array[m] > value)
            {
                maxi = m - 1;
            }
            else
            {
                mini = m + 1;
            }
        }
    }
    return -1;

}