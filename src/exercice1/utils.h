/**
 * file : utils.h
 * author : albane castillon
 * date : 20/10/2021
 * version : 1
*/

/**
 * @brief créé un tableau avec des valeurs random
 * 
 * @param tab tableau de valeur int
 * @param taille taille du tableau 
 */
void filltab(int *tab, int taille);

void swap(int *a, int *b);

/**
 * @brief tri les valeurs par avec le tri par tas
 * 
 * @param tab tableau int a trier
 * @param size taille du tableau
 * @param i 
 */
void heapify(int *tab, int size, int i);

/**
 * @brief tri les valeurs par avec le tri par tas
 * 
 * @param tab tableau int a trier
 * @param size taille du tableau
 */
void heapsort(int *tab, int size);

