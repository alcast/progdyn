/**
 * file : main.c
 * author : albane castillon
 * date : 20/10/2021
 * version : 1
*/

#include <stdio.h>
#include <stdlib.h>
#include "exercice1/utils.h"
#include "exercice1/dichotomie.h"
#include "exercice2/Knapsack.h"

int main()
{
	// int array[10]= {2,4,6,8,11,15,18,20,24,26};

	// int value = 20;
	// int size = 100;
	// //int array[size];
	// fillandsorttab(array, size);
	// for (int i = 0; i < size; i++)
	// {
	// 	printf ("%i ", array[i]);
	// }

	// int resultat = find_by_dichotomy(array, size, value);
	// printf ("le resultat est : %i \n", resultat);

	int poidsmax = 10;

	Objet obj1, obj2, obj3;
	obj1.poids = 6;
	obj1.valeur = 7;

	obj2.poids = 5;
	obj2.valeur = 5;

	obj3.poids = 5;
	obj3.valeur = 5;

	Objet objettab[3] = {obj1, obj2, obj3};
	Objet *sac = Knapsack(objettab, poidsmax, 3);

	for (int i = 0; i < 3; i++)
	{
		printf("\nL'objet %i dans le sac: \n", i);
		printtri(&sac[i]);
	}

	return (0);
}
