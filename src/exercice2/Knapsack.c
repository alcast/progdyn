/**
 * file : Knapsack.c
 * author : albane castillon
 * date : 20/10/2021
 * version : 1
*/

#include "Knapsack.h"
#include <stdio.h>
#include <stdlib.h>

/**
 * @brief tri les valeurs par avec le tri d'insertion
 * 
 * @param object tableau contenant les objets à trier
 * @param size taille du tableau d'objet
*/ 
void tri(Objet objet[], int size)
    {
        int i, j;
        Objet temporaire;

        for (i = 1; i < size-1; i++)
        {
            temporaire = objet[i];
            j = i - 1;
            while ((objet[j].valeur / objet[j].poids < (temporaire.valeur / temporaire.poids) && j >= 0))
            {
                objet[j + 1] = objet[j];
                j--;
            }
            objet[j + 1] = temporaire;
        }

    }

/**
 * @brief la fonction va mettre les objets dans le sac
 * 
 * @param objet tableau contenant les objets
 * @param capacite int poids max que le sac peut contenir
 * @param taille int taille des objets
*/
Objet *Knapsack(Objet objets[], int capacite, int taille) 
{
    Objet *sac=malloc(50*sizeof(Objet));
    tri(objets, taille);

    for (int i = 0; i < taille; i++)
    {
        if (capacite - objets[i].poids >= 0)
        {
            sac[i] = objets[i];
            capacite = capacite - objets[i].poids;
        }
    }
    return sac;
}



/**
 * @brief affiche les valeurs des parametres de l'objets
 * @param objet contient la valeur et le poids
 */

void *printtri(Objet objet[])
{
    printf("Le poids est : %0.f\n", objet->poids);
    printf("La valeur est : %0.f\n", objet->valeur);
}
    
