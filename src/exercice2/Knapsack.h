/**
 * file : Knapsack.h
 * author : albane castillon
 * date : 20/10/2021
 * version : 1
*/

typedef struct
{
    float poids;
    float valeur;
} Objet;

/**
 * @brief la fonction va mettre les objets dans le sac
 * 
 * @param objet tableau contenant les objets
 * @param capacite int poids max que le sac peut contenir
 * @param taille int taille des objets
*/
Objet *Knapsack(Objet objets[], int capacite, int taille); 

/**
 * @brief tri les valeurs par avec le tri d'insertion
 * 
 * @param object tableau contenant les objets à trier
 * @param size taille du tableau d'objet
*/ 
void tri(Objet objet[], int size);
